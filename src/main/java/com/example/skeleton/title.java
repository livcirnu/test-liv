package com.example.skeleton;

import com.google.appengine.api.datastore.Key;

public class title {

	private Key key;
	private String text;

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}
	
	public String getMessage() {
		return this.text;
	}
	
	public void setMessage(String text) {
		this.text = text;
	}
}