package com.example.skeleton;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

public class Auth {

	public String userId;
	
	public boolean Verify(String idTokenString) {
		try {
			ApacheHttpTransport.Builder builder = new ApacheHttpTransport.Builder();

			GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(builder.build(), new JacksonFactory())
			    .setAudience(Arrays.asList("32800998361-nqq968mnq3csjm19khnqqqtq2mceg764.apps.googleusercontent.com", "32800998361-nqq968mnq3csjm19khnqqqtq2mceg764"))
			    .setIssuer("accounts.google.com")
			    .build();
	
			GoogleIdToken idToken = verifier.verify(idTokenString);
	
			if (idToken == null) {
				return false;
			}
			
			Payload payload = idToken.getPayload();

			// Save user's unique Id
			this.userId = payload.getSubject();
			
			return true;
		} catch (GeneralSecurityException | IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
