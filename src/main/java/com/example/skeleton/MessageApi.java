/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.skeleton;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@Api(name = "message", version = "v1", namespace = @ApiNamespace(ownerDomain = "message.example.com", ownerName = "message.example.com", packagePath = ""))
public class MessageApi {

	private final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	// private final Auth auth = new Auth();
	
	private Message entityToMessage(final Entity e) {
		
		String title = (String) e.getProperty("title");
		String body = (String) e.getProperty("message");
		String author = (String) e.getProperty("author");
		long id = e.getKey().getId();
		Date creationDate = (Date) e.getProperty("creationDate");
		Message m = new Message();
		m.setTitle(title);
		m.setAuthor(author);
		m.setCreationDate(creationDate == null ? null : DateTimeFormatter.ISO_INSTANT.format(creationDate.toInstant()));
		m.setId(String.valueOf(id));
		m.setBody(body);
		
		return m;
	}

	@ApiMethod(name = "get", path = "get/{id}", httpMethod = ApiMethod.HttpMethod.GET)
	public Message get(final @Named("id") long id) throws EntityNotFoundException {
//		if (!this.auth.Verify(idTokenString)) {
//			return;
//		}

		Key toGet = KeyFactory.createKey("Message", id);
		
		Entity msg = this.datastore.get(toGet);
		return entityToMessage(msg);
		
	}
	
	@ApiMethod(name = "create", path = "create", httpMethod = ApiMethod.HttpMethod.POST)
	public Message create(final Message message) throws EntityNotFoundException {
//		if (!this.auth.Verify(idTokenString)) {
//			return;
//		}

		Entity newMessage = new Entity("Message");
		newMessage.setProperty("author", message.getAuthor());
		newMessage.setProperty("title", message.getTitle());
		newMessage.setProperty("message", message.getBody());
		newMessage.setProperty("creationDate", new Date());

		Key newMessageKey = this.datastore.put(newMessage);
		
		Entity savedMessage = this.datastore.get(newMessageKey);
		return entityToMessage(savedMessage);
	}

	@ApiMethod(name = "delete", path = "delete/{id}", httpMethod = ApiMethod.HttpMethod.DELETE)
	public void delete(final @Named("id") long id) throws EntityNotFoundException {
//		if (!this.auth.Verify(idTokenString)) {
//			return;
//		}

		Key toDelete = KeyFactory.createKey("Message", id);
		
		this.datastore.delete(toDelete);
		
	}


	 @ApiMethod(
		name = "update", path = "update/{id}",
		httpMethod = ApiMethod.HttpMethod.PUT)
	public Message update(final @Named("id") long id, final Message newMessage) throws EntityNotFoundException {
		 Key parentKey = KeyFactory.createKey("Message", id);
		 
		 Entity oldMessage = datastore.get(parentKey);
		 oldMessage.setProperty("title", newMessage.getTitle());
		 oldMessage.setProperty("message", newMessage.getBody());
		 
		 Key newMessageKey = this.datastore.put(oldMessage);
		 
		 
		Entity updatedMessage = this.datastore.get(newMessageKey);
		return entityToMessage(updatedMessage);
	}

	@ApiMethod(name = "list", path = "list", httpMethod = ApiMethod.HttpMethod.GET)
	public MessagesResponse list() {
//		if (!this.auth.Verify(idTokenString)) {
//			return null;
//		}

		Query q = new Query("Message");

//		List<Entity> results =
//		    datastore.prepare(messageQuery).asList(FetchOptions.Builder.withDefaults())

		

		PreparedQuery pq = datastore.prepare(q);
		
		List<Message> c = new ArrayList<Message>();
		for (Entity result : pq.asIterable()) {
			c.add(entityToMessage(result));
		}

		MessagesResponse mresp = new MessagesResponse();
		mresp.setMessages(c);

		return mresp;
	}

	@ApiMethod(
			name = "comment", path = "comment",
			httpMethod = ApiMethod.HttpMethod.POST)
		public void comment(@Named("parentKey") String key, @Named("user") String userEmail, @Named("comment") String comment) throws EntityNotFoundException {
//			if (!this.auth.Verify(idTokenString)) {
//				return;
			Key parentKey = KeyFactory.stringToKey(key);
		
			Entity newComment = new Entity("Comment", parentKey);
			newComment.setProperty("author", userEmail);
			newComment.setProperty("message",comment);
			newComment.setProperty("date", new Date());
			datastore.put(newComment);
			
//			List<Key> messages = new ArrayList<Key>();
//			messages.add(messages.getKey());
//			newComment.setProperty("MessagesList", messages);
			
	//
			
	}
}
