package com.example.skeleton;

import com.google.appengine.api.datastore.Key;

public class Comment {

	private Key key;
	private String text;

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}
	
	public String getComment() {
		return this.text;
	}
	
	public void setComment(String text) {
		this.text = text;
	}
}
